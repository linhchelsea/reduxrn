import React, {Component} from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {decrease, increase, setValue} from "./Action";
import DisplayView from "./view/DisplayView";
import InputView from "./view/InputView";
import Button from "./view/Button";

class SimpleDemo extends Component {

    render() {
        return (
            <View style={styles.container}>
                <DisplayView/>
                <InputView/>
                <Button/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

const mapStateToProps = (state) => {
    return {}
};

const mapActionToProps = (dispatch) => (
    bindActionCreators({increase, decrease, setValue}, dispatch)
);

export default connect(mapStateToProps, mapActionToProps)(SimpleDemo);


