import React, {Component} from 'react';
import {
    StyleSheet, TouchableOpacity, Text, View
} from 'react-native';

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {decrease, increase} from "../Action";

class Button extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={[styles.button, styles.button_increase]}
                    onPress={this.onIncreasePress}
                >
                    <Text style={styles.text_button}>INCREASE</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.button, styles.button_decrease]}
                    onPress={this.onDecreasePress}
                >
                    <Text style={styles.text_button}>DECREASE</Text>
                </TouchableOpacity>
            </View>
        );
    }

    onIncreasePress = () => {
        this.props.increase();
    };

    onDecreasePress = () => {
        this.props.decrease();
    };
}

/**
 * Chuyen action sang props cua component
 * @param dispatch
 * @returns {{increase: increase, decrease: decrease}}
 */
const mapAction2Props = (dispatch) => {
    return bindActionCreators({increase, decrease}, dispatch);
};

// export default connect(null, mapAction2Props)(Button);
export default connect(null, mapAction2Props)(Button);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: '30%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button_increase: {
        backgroundColor: '#796b63'
    },
    button_decrease: {
        backgroundColor: '#797739'
    },
    text_button: {
        fontSize: 15,
        color: '#ffffff'
    }
});



