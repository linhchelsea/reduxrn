
import {createStore} from "redux";

export const defaultState = {
    value: 0,
};

export const simpleReducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'INCREASE':
            return {value: state.value + 1};
        case 'DECREASE':
            return {value: state.value - 1};
        case 'SET':
            return {value: action.value};
        default:
            return state;
    }
};

export const store = createStore(simpleReducer);


