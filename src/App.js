import React, {Component} from 'react';
import List from "./List";

type Props = {};
const data = [
    {id:1,name: 'Android'},
    {id:2, name: 'iOS'},
    {id:3,name: 'React JS'},
    {id:4,name: 'React native'},
    {id:5,name: 'Windows'},
    {id:6,name: 'NodeJS'},
    {id:7,name: 'Java'},
    {id:8,name: 'Xamarin'},
];

export default class App extends Component<Props> {
    render() {
        return (
            <List data={data}/>
        );
    }
}

