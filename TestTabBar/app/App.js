import React, {Component} from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { TabBarBottom, createBottomTabNavigator } from 'react-navigation';

type Props = {};

class Screen1 extends Component<Props> {
  static navigationOptions = {
    title: 'f a c e',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={[styles.instructions, { color: '#ff0000'}]}>To get started, edit App.js</Text>
      </View>
    );
  }
}

class Screen2 extends Component<Props> {
   static navigationOptions = {
    title: 'i n s t a',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={[styles.instructions,{ color: '#000fff'}]}>To get started, edit App.js</Text>
      </View>
    );
  }
}

class Screen3 extends Component<Props> {
   static navigationOptions = {
    title: 'g m a i l',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={[styles.instructions,{ color: '#000000'}]}>To get started, edit App.js</Text>
      </View>
    );
  }
}

export default createBottomTabNavigator({
  Home: { screen: Screen1 },
  House: { screen: Screen2 },
  Gmail: { screen: Screen3 },
},{
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, tinColor }) => {
      const { routeName } = navigation.state;
      let icon;
      if (routeName === 'Home') {
        icon = require('../asset/images/face.png')
      } else if (routeName === 'Houes') {
        icon = require('../asset/images/insta.png');
      } else {
        icon = require('../asset/images/gmail.png');
      }
      return <Image
        style={{ width: 20, height: 20, }}
        source={ icon }
      />
    }
  }),
  tabBarOptions: {
    activeTintColor: 'tomato',
    inactiveTintColor: 'gray',
    indicatorStyle: {
      backgroundColor: '#ff0000'
    }
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    marginBottom: 5,
  },
});
