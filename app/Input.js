import React, {Component} from 'react';
import {TextInput, TouchableOpacity, StyleSheet, Text, View} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setValue } from './Action';

type Props = {};

class InputView extends Component<Props> {
  constructor (props) {
    super(props);
    this.state = {
      value: 0,
    };
  }
  render() {
    return (
      <View style={ styles.container }>
        <TextInput
          ref={ r => this.input = r }
          style={ styles.txt_input }
          placeholder={ 'Input number' }
          keyboardType={ 'numeric' }
          onChangeText={ this.onValueChange }
        />
        <TouchableOpacity
          style={ styles.btn_input }
          onPress={ this.onPress }
        >
          <Text style={ styles.txt_btn }>SET VALUE</Text>
        </TouchableOpacity>
      </View>
    );
  }

  onPress = () => {
    if (this.state.value) {
      this.props.setValue(this.state.value);
      this.input.clear();
    }
  };

  onValueChange = (text) => {
    try {
      const value = +text;
      this.setState({ value });
    } catch (e) {
      alert(e);
    }
  }
}

const mapActionToProps = { setValue };

export default connect(null, mapActionToProps)(InputView);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  txt_input: {
    width: '100%',
    height: 50,
    backgroundColor: '#ffffff',
    textAlign: 'center',
    marginBottom: 30,
  },
  btn_input: {
    width: '100%',
    height: 40,
    backgroundColor: '#737d29',
    justifyContent: 'center',
    alignItems: 'center'
  },
  txt_btn: {
    color: '#ffffff',
  },
});
