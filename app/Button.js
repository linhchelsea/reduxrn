import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet, Text, View} from 'react-native';
import { connect } from 'react-redux';
import { increase, decrease } from './Action';

type Props = {};

class Button extends Component<Props> {
  constructor (props) {
    super(props);
  }

  render() {
    return (
      <View style={ styles.container }>
        <TouchableOpacity
          style={ [styles.btn_action, styles.btn_inc] }
          onPress={ this.onIncreasePress }
        >
          <Text style={ styles.txt_btn }>INCREASE</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={ [styles.btn_action, styles.btn_dec] }
          onPress={ this.onDecreasePress }
        >
          <Text style={ styles.txt_btn }>DECREASE</Text>
        </TouchableOpacity>
      </View>
    );
  }

  onIncreasePress = () => {
    this.props.increase();
  };

  onDecreasePress = () => {
    this.props.decrease();
  };
}
const mapActionToProps = { increase, decrease };

export default connect(null, mapActionToProps)(Button);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  txt_btn: {
    color: '#ffffff',
  },
  btn_action: {
    width: '50%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn_inc: {
    backgroundColor: '#786b62',
  },
  btn_dec: {
    backgroundColor: '#737d29'
  },
});