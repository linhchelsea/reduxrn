import { createStore } from 'redux';

export const defaultState = {
  value: 0,
};

export const simpleReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'INCREASE':
      return { ...state, value: state.value + 1 };
      break;
    case 'DECREASE':
      return { ...state, value: state.value - 1 };
      break;
    case 'SET':
      return { ...state, value: action.value };
      break;
    default:
      return state;
  }
};

export const store = createStore(simpleReducer);