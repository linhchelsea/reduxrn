import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { store } from './Reducer';
import App from './App';

export default class AppProvider extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <Provider store={ store }>
        <App/>
      </Provider>
    )
  }
}
