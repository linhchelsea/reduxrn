import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
type Props = {};

class Display extends Component<Props> {
  constructor (props) {
    super(props);
  }

  render() {
    return (
      <View style={ styles.container }>
        <Text style={ styles.txt_display }>Value: { this.props.index }</Text>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    index: state.value,
  };
};

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(Display);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  txt_display: {
    fontSize: 40,
    color: '#778131',
  },
});
