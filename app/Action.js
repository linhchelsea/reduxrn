export const increase = () => {
  return {
    type: 'INCREASE',
  };
};

export const decrease = () => {
  return {
    type: 'DECREASE',
  };
};

export const setValue = (value) => {
  return {
    type: 'SET',
    value,
  };
};
