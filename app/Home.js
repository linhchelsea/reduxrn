import React, {Component} from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { increase, decrease, setValue } from './Action';

import Button from './Button';
import Display from './Display';
import InputView from './Input';

class Home extends Component {
  render() {
    return (
      <View style={ styles.container }>
        <Display/>
        <InputView/>
        <Button/>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {};
};

const mapActionToProps = { increase, decrease, setValue };

export default connect(null, mapActionToProps)(Home);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
